# temporary



## Getting started - Install

First you need to make sure you have `conda` or `mamba` installed.
Then you should `cd` to the directory in which you want to clone your repository in.

1. Clone the git:
```
git clone https://gitlab.com/mactodd315/temporary_657
```

2. Setup the environment
```
cd temporary_657
conda env create -f environment.yml
```
3. activate environment
```
conda activate phy657
```

4. Now you should be able to run jupyter commands or use the phy657 kernal directly in an IDE like VSCode.

For example, to open the jupyter notebook file module1.ipynb, you must have this environment active, then run:
```
jupyter notebook module1.ipynb
```

